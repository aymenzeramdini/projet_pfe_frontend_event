import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class MailService {
  constructor(private http: HttpClient) { }

  
  send(details): Observable<any> {
    return this.http.post('http://localhost:8080/feedback/feed', {
      name: details.name,
      email: details.email,
      subject: details.subject,
      feedback:details.feedback

    }, httpOptions);
  }
}
