import { Time } from '@angular/common';
import { User } from './user';
import { Observable } from 'rxjs';

export class Event {
    idEvent: number;
    nbparticipant: number;
    sport: string;
    adresse:string;
    duree:number;
    prix:number;
    heure_debut:Time;
    date:String;
    nbplace:number;
    niveau: string;
    createur:User;
    participants:Observable<User[]>;
}