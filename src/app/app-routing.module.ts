import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';

import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { AllusersComponent } from './components/allusers/allusers.component';
import { ProfileComponent } from './components/profile/profile.component';
import { OrgeventComponent } from './components/orgevent/orgevent.component';
import { ListeventComponent } from './components/listevent/listevent.component';
import { EventdetailComponent } from './components/eventdetail/eventdetail.component';
import { UpdateComponent } from './components/update/update.component';
import { UserdetailComponent } from './components/userdetail/userdetail.component';
import { MeseventsComponent } from './components/mesevents/mesevents.component';
import { AlleventsComponent } from './components/allevents/allevents.component';
import { EventdetailcreateurComponent } from './components/eventdetailcreateur/eventdetailcreateur.component';


const routes: Routes = [
  {path:"",component:HomeComponent},
  {path:"home",component:HomeComponent},
  { path:"register", component: RegisterComponent },
  { path:"login", component: LoginComponent },
  { path:"orgevent", component: OrgeventComponent },
  { path:"listevent", component: ListeventComponent },
  { path:"allusers", component: AllusersComponent },
  { path:"allevents", component: AlleventsComponent },

  { path:"eventdetail/:id", component: EventdetailComponent },
  { path:"eventdetailcreateur/:id", component: EventdetailcreateurComponent },

  { path:"profile", component: ProfileComponent },
  { path:"update/:id", component: UpdateComponent },
  { path:"userdetail/:id", component: UserdetailComponent },
  { path:"mesevents", component: MeseventsComponent },


  
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
