import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { UserService } from "../../user.service";
import { Router } from '@angular/router';
import { User } from "../../user";

@Component({
  selector: 'app-allusers',
  templateUrl: './allusers.component.html',
  styleUrls: ['./allusers.component.css']
})
export class AllusersComponent implements OnInit {

  Users: Observable<User[]>;

  constructor(private userService: UserService,
    private router: Router) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.Users = this.userService.getUsers();
  }

  deleteUser(user_id: number) {
    this.userService.deleteUser(user_id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  userDetails(user_id: number){
    this.router.navigate(['userdetail', user_id]);
  }
  updateUser(user_id: number){
    this.router.navigate(['update', user_id]);
  }
  

}
