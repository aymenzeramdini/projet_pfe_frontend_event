import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrgeventComponent } from './orgevent.component';

describe('OrgeventComponent', () => {
  let component: OrgeventComponent;
  let fixture: ComponentFixture<OrgeventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrgeventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrgeventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
