import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../_services/auth.service';
import { TokenStorageService } from '../../_services/token-storage.service';

@Component({
  selector: 'app-orgevent',
  templateUrl: './orgevent.component.html',
  styleUrls: ['./orgevent.component.css']
})
export class OrgeventComponent implements OnInit {
  form: any = {};
  errorMessage = '';
  isSuccessful = false;
  currentUser: any;
  constructor(private authService: AuthService,private token: TokenStorageService) { }

  ngOnInit(){
    this.currentUser = this.token.getUser();

  }
  onSubmit() {
    this.authService.orgevent(this.form).subscribe(
    data => {
      console.log(data);
      this.isSuccessful = true;

      
    },
    err => {
      this.errorMessage = err.error.message;
     
}
);
}}
