import { Component, OnInit } from '@angular/core';
import { UserService } from "../../user.service";
import { Router, ActivatedRoute } from '@angular/router';
import { Event } from "../../event";
import { TokenStorageService } from 'src/app/_services/token-storage.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from 'src/app/user';
@Component({
  selector: 'app-eventdetail',
  templateUrl: './eventdetail.component.html',
  styleUrls: ['./eventdetail.component.css']
})
export class EventdetailComponent implements OnInit {
id:number;
event:Event;
currentUser: any;
errorMessage = '';
Failed=false;
Sucess=false;
Sremove=false;
Failedremove=false;
Users: Observable<User[]>;

constructor(private userService: UserService,
  private router: Router,private token: TokenStorageService ,private route: ActivatedRoute,private http: HttpClient) {}

  ngOnInit() {
    this.currentUser = this.token.getUser();

    this.event=new Event();
    this.id = this.route.snapshot.params['id'];
    
    this.userService.getEvent(this.id)
      .subscribe(data => {
        console.log(data)
        this.event = data;
      }, error => console.log(error));
  }

  list(){
    this.router.navigate(['listevent']);
  }
  participer(id_event:number,id_user:number)
  {
    this.userService.addparticipant(id_event,id_user ,this.event)
    .subscribe(data => {console.log(data);this.Sucess = true ;window.location.reload();
    },
    error => {this.errorMessage = error.error.message;
      this.Failed = true;
    }
      ); }
 
}
