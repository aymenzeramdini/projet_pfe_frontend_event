import { Component, OnInit } from '@angular/core';
//import { UserService } from '../../_services/user.service';
import { TokenStorageService } from '../../_services/token-storage.service';
import { HttpClient } from '@angular/common/http';
import { MailService } from '../../_services/mail.service';
import { AuthService } from '../../_services/auth.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  content: string;
  private roles: string[];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  username: string;
  form: any = {};
  msgerr = false;
  sucess = false;

  
  constructor(private authService: AuthService,private tokenStorageService: TokenStorageService,private https: HttpClient) { }

  ngOnInit() {
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');

      this.username = user.username;
    }
    
  }
 onSubmit(){
    this.authService.send(this.form).subscribe(
    res => {
    this.form = res;
    console.log(this.form);
    this.sucess = true;
    alert('Email Sent successfully');
    window.location.reload();
    this.form.name = '';
    this.form.subject = '';
    this.form.feedback = '';
    this.form.email = '';
  },
  err => {
    this.msgerr = true;
  }
    );
    }



  logout() {
    this.tokenStorageService.signOut();
    window.location.reload();
  }
}
