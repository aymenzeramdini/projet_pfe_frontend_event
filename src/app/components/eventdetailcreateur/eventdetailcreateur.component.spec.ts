import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventdetailcreateurComponent } from './eventdetailcreateur.component';

describe('EventdetailcreateurComponent', () => {
  let component: EventdetailcreateurComponent;
  let fixture: ComponentFixture<EventdetailcreateurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventdetailcreateurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventdetailcreateurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
