import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TokenStorageService } from 'src/app/_services/token-storage.service';
import { HttpClient } from '@angular/common/http';
import { Event } from "../../event";
@Component({
  selector: 'app-eventdetailcreateur',
  templateUrl: './eventdetailcreateur.component.html',
  styleUrls: ['./eventdetailcreateur.component.css']
})
export class EventdetailcreateurComponent implements OnInit {
  id:number;
  event:Event;
  currentUser: any;
  errorMessage:"";
  Failedremove=false;
  constructor(private userService: UserService,
    private router: Router,private token: TokenStorageService ,private route: ActivatedRoute,private http: HttpClient) { }

  ngOnInit(): void {this.currentUser = this.token.getUser();

    this.event=new Event();
    this.id = this.route.snapshot.params['id'];
    
    this.userService.getEvent(this.id)
      .subscribe(data => {
        console.log(data)
        this.event = data;
      }, error => console.log(error));
  }
  remove(id_event:number)
  {
    this.userService.deleteevent(id_event)
    .subscribe(
      data => {console.log(data);
        this.router.navigate(['mesevents']);

    }, 
    error => {
      this.errorMessage = error.error.message;
      this.Failedremove = true;

    }
      ); 
  } 

  list(){
    this.router.navigate(['mesevents']);
  }
}
