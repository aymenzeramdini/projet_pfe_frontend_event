import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeseventsComponent } from './mesevents.component';

describe('MeseventsComponent', () => {
  let component: MeseventsComponent;
  let fixture: ComponentFixture<MeseventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeseventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeseventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
