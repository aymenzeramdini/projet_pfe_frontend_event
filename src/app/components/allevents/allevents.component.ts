import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/user.service';
import { Router } from '@angular/router';
import { TokenStorageService } from 'src/app/_services/token-storage.service';

@Component({
  selector: 'app-allevents',
  templateUrl: './allevents.component.html',
  styleUrls: ['./allevents.component.css']
})
export class AlleventsComponent implements OnInit {
  Events: Observable<Event[]>;
  errorMessage:"";
  constructor(private userService: UserService,
    private router: Router,private token: TokenStorageService) {}
    currentUser: any;
    a:any;
  ngOnInit() {
    this.reloadData();
    this.currentUser = this.token.getUser();

  }

  reloadData() {
    this.Events = this.userService.getEvents();
  }
  remove(id_event:number)
  {
    this.userService.deleteevent(id_event)
    .subscribe(data => {console.log(data);
      this.reloadData();}, 
    error => {
      this.errorMessage = error.error.message;
    }
      ); 
  } 

}
