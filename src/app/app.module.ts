import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { AllusersComponent } from './components/allusers/allusers.component';
import { ProfileComponent } from './components/profile/profile.component';
import { OrgeventComponent } from './components/orgevent/orgevent.component';
import { ListeventComponent } from './components/listevent/listevent.component';
import { EventdetailComponent } from './components/eventdetail/eventdetail.component';
import { UpdateComponent } from './components/update/update.component';
import { UserdetailComponent } from './components/userdetail/userdetail.component';
import { authInterceptorProviders } from './_helper/auth.interceptor.service';
import { MeseventsComponent } from './components/mesevents/mesevents.component';
import { AlleventsComponent } from './components/allevents/allevents.component';
import { EventdetailcreateurComponent } from './components/eventdetailcreateur/eventdetailcreateur.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    
    RegisterComponent,
    
    LoginComponent,
    

    
    AllusersComponent,
    
    ProfileComponent,
    
    OrgeventComponent,
    
    ListeventComponent,
    
    EventdetailComponent,
    
    UpdateComponent,
    
    UserdetailComponent,
    
    MeseventsComponent,
    
    AlleventsComponent,
    
    EventdetailcreateurComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
